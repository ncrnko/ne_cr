module contact-form.example.com

go 1.17

require (
	github.com/bmizerany/pat v0.0.0-20210406213842-e4b6760bdd6f
	github.com/go-mail/mail v2.3.1+incompatible
)

require (
	github.com/go-session/session/v3 v3.1.5 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/mail.v2 v2.3.1 // indirect
)
