package main

import (
	"html/template"
	"log"
	"net/http"

	"github.com/bmizerany/pat"
)

type UserData struct {
	username  string
	password  string
	fullname  string
	telephone string
	address   string
}

var GlobalError string = ""
var ConnectionString string = ""
var OneUser UserData

func main() {
	getConnectionString()
	checkConnection()

	mux := pat.New()

	mux.Get("/", http.HandlerFunc(login))
	mux.Get("/login", http.HandlerFunc(login))
	mux.Post("/login", http.HandlerFunc(loginProcess))
	mux.Get("/signup", http.HandlerFunc(signup))
	mux.Post("/signup", http.HandlerFunc(signupProcess))
	mux.Get("/forgot-password", http.HandlerFunc(forgotPassword))
	mux.Post("/forgot-password", http.HandlerFunc(forgotPasswordProcess))
	mux.Get("/reset-password", http.HandlerFunc(resetPassword))
	mux.Post("/reset-password", http.HandlerFunc(resetPasswordProcess))
	mux.Get("/main-profile", http.HandlerFunc(mainProfile))
	mux.Get("/edit", http.HandlerFunc(editProfile))
	mux.Post("/edit-save", http.HandlerFunc(editProfileSave))
	mux.Get("/edit-continue", http.HandlerFunc(editProfileCancel))
	mux.Get("/logout", http.HandlerFunc(logout))

	log.Println("Go Challenge server is ready...")
	log.Println("Starting point http://localhost:8080/ ")
	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatal(err)
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	data := struct{ ErrMsg string }{ErrMsg: GlobalError}
	render(w, "templates/login.html", data)
}

func loginProcess(w http.ResponseWriter, r *http.Request) {
	GlobalError = ""
	status := getUser(r.PostFormValue("email"), r.PostFormValue("password"), w, r)
	if status == "" {
		GlobalError = "Invalid authentication!"
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		http.Redirect(w, r, "/main-profile", http.StatusFound)
	}
}

func signup(w http.ResponseWriter, r *http.Request) {
	render(w, "templates/signup.html", nil)
}

func signupProcess(w http.ResponseWriter, r *http.Request) {
	GlobalError = ""
	if r.PostFormValue("password") != r.PostFormValue("confirmation") {
		GlobalError = "The Password and the password confirmation are not the same!"
		data := struct{ ErrMsg string }{ErrMsg: GlobalError}
		render(w, "templates/signup.html", data)
		return
	}

	status := existingUser(r.PostFormValue("email"), w, r)
	if status != "" {
		GlobalError = "The user " + r.PostFormValue("email") + " already exists in the database!"
		data := struct{ ErrMsg string }{ErrMsg: GlobalError}
		render(w, "templates/signup.html", data)
		return
	}

	insertUser(r.PostFormValue("email"), r.PostFormValue("password"), w, r)
	http.Redirect(w, r, "/edit", http.StatusFound)
}

func mainProfile(w http.ResponseWriter, r *http.Request) {
	if sessionRead("username", w, r) == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	data := struct {
		UserName  string
		Fullname  string
		Telephone string
		Address   string
	}{
		UserName:  OneUser.username,
		Fullname:  OneUser.fullname,
		Telephone: OneUser.telephone,
		Address:   OneUser.address,
	}
	render(w, "templates/main-profile.html", data)
}

func editProfile(w http.ResponseWriter, r *http.Request) {
	if sessionRead("username", w, r) == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	data := struct {
		UserName  string
		Fullname  string
		Telephone string
		Address   string
	}{
		UserName:  OneUser.username,
		Fullname:  OneUser.fullname,
		Telephone: OneUser.telephone,
		Address:   OneUser.address,
	}
	render(w, "templates/edit-profile.html", data)
}

func editProfileSave(w http.ResponseWriter, r *http.Request) {
	if sessionRead("username", w, r) == "" {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	GlobalError = ""
	_ = updateUser(r.PostFormValue("username"),
		r.PostFormValue("fullname"),
		r.PostFormValue("telephone"),
		r.PostFormValue("address"),
		w, r)
	http.Redirect(w, r, "/main-profile", http.StatusFound)
}

func editProfileCancel(w http.ResponseWriter, r *http.Request) {
	if sessionRead("username", w, r) == "" {
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		http.Redirect(w, r, "/main-profile", http.StatusFound)
	}
}

func forgotPassword(w http.ResponseWriter, r *http.Request) {
	render(w, "templates/forgot-password.html", nil)
}

func forgotPasswordProcess(w http.ResponseWriter, r *http.Request) {
	status := existingUser(r.PostFormValue("email"), w, r)
	if status == "" {
		GlobalError = "The mail " + r.PostFormValue("email") + " does not exists in the database!"
		data := struct{ ErrMsg string }{ErrMsg: GlobalError}
		render(w, "templates/forgot-password.html", data)
		return
	}
	sendLink(r.PostFormValue("email"), w, r)
	GlobalError = "The reset link was sent to " + r.PostFormValue("email")
	data := struct{ ErrMsg string }{ErrMsg: GlobalError}
	render(w, "templates/forgot-password.html", data)
}

func resetPassword(w http.ResponseWriter, r *http.Request) {
	resetKey := r.URL.Query().Get("resetkey")
	username := getUserFromHash(resetKey, w, r)
	if username == "" {
		GlobalError = "Invalid password reset link!"
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		data := struct {
			UserName string
			ErrMsg   string
		}{
			UserName: username,
			ErrMsg:   "",
		}
		render(w, "templates/reset-password.html", data)
	}
}

func resetPasswordProcess(w http.ResponseWriter, r *http.Request) {
	username := r.PostFormValue("email")
	password := r.PostFormValue("password")
	_ = updatePassword(username, password, w, r)
	GlobalError = "Password changed successfully."
	http.Redirect(w, r, "/", http.StatusFound)
}

func logout(w http.ResponseWriter, r *http.Request) {
	sessionDestroy(w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func render(w http.ResponseWriter, filename string, data interface{}) {
	tmpl, err := template.ParseFiles(filename)
	if err != nil {
		log.Println(err)
		http.Error(w, "Sorry, something went wrong", http.StatusInternalServerError)
		return
	}

	if err := tmpl.Execute(w, data); err != nil {
		log.Println(err)
		http.Error(w, "Sorry, something went wrong 2", http.StatusInternalServerError)
	}
}
