package main

import (
	"log"
	"net/http"
	"net/smtp"
	// "github.com/go-mail/mail"
)

func sendLink(email string, w http.ResponseWriter, r *http.Request) {
	link := prepareResetLink(email, w, r)
	_ = link

	from := "go@challenge.com"
	to := "email"
	host := "smtp.somehost.host"   // need real email host
	hostUsername := "someusername" // need real username for host
	hostPassword := "someusername" // need real password for host

	auth := smtp.PlainAuth("", hostUsername, hostPassword, host)
	message := "<a href='http://localhost:8080/reset-password?resetkey=" + link + "'>Reset password</a>"
	//log.Println(message)

	if err := smtp.SendMail(host+":587", auth, from, []string{to}, []byte(message)); err != nil {
		log.Println(err)
	}
}
