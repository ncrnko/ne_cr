package main

import (
	"database/sql"
	"hash/fnv"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

func getConnectionString() {
	content, err := ioutil.ReadFile("gochallenge.ini")
	if err != nil {
		log.Fatal("Cannot read connection string from file gochallenge.ini!")
	}

	ConnectionString = string(content)
}

func checkConnection() {
	_, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

}

func hash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

func hashPassword(password string) string {
	return (strconv.FormatUint(uint64(hash(password)), 10))
}

func hashLink(email string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(ConnectionString), 6)
	return string(bytes)
}

func clearUser() {
	OneUser.username = ""
	OneUser.password = ""
	OneUser.fullname = ""
	OneUser.telephone = ""
}

func existingUser(username string, w http.ResponseWriter, r *http.Request) string {
	status := false
	clearUser()

	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	dbSQL := "select username from users where username = '" + username + "'"
	results, err := dbCN.Query(dbSQL)
	if err != nil {
		log.Fatal(err)
	}

	for results.Next() {
		err = results.Scan(&OneUser.username)
		if err != nil {
			log.Fatal(err)
		}
		status = true
	}

	if status == true {
		sessionSave("username", username, w, r)
	}

	return OneUser.username

}

func getUser(username string, password string, w http.ResponseWriter, r *http.Request) string {
	status := false
	clearUser()

	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	password = hashPassword(password)
	dbSQL := "select username, password, fullname, telephone, address from users where username = '" + username + "' and password = '" + password + "'"
	results, err := dbCN.Query(dbSQL)
	if err != nil {
		log.Fatal(err)
	}

	for results.Next() {
		err = results.Scan(
			&OneUser.username,
			&OneUser.password,
			&OneUser.fullname,
			&OneUser.telephone,
			&OneUser.address,
		)
		if err != nil {
			log.Fatal(err)
		}
		status = true
	}

	if status == true {
		sessionSave("username", username, w, r)
	}

	return OneUser.username

}

func getUserFromHash(hash string, w http.ResponseWriter, r *http.Request) string {
	status := false

	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	dbSQL := "select username from users where hashlink = '" + hash + "' and expires >= NOW()"
	results, err := dbCN.Query(dbSQL)
	if err != nil {
		log.Fatal(err)
	}

	for results.Next() {
		clearUser()
		err = results.Scan(&OneUser.username)
		if err != nil {
			log.Fatal(err)
		}
		status = true
	}

	if status == true {
		return OneUser.username
	} else {
		return ""
	}

}

func insertUser(username string, password string, w http.ResponseWriter, r *http.Request) string {
	clearUser()
	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	password = hashPassword(password)
	dbSQL := "insert into users (username, password) values('" + username + "','" + password + "')"
	results, err := dbCN.Exec(dbSQL)
	_ = results
	if err != nil {
		log.Fatal("Error insert record in database with SQL Command -> " + dbSQL)
	}

	OneUser.username = username
	sessionSave("username", username, w, r)

	return ""
}

func updateUser(username string, fullname string, telephone string, address string, w http.ResponseWriter, r *http.Request) string {
	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	dbSQL := "update users set fullname = '" + fullname + "', telephone = '" + telephone + "', address = '" + address + "' where username = '" + username + "'"
	results, err := dbCN.Exec(dbSQL)
	_ = results
	if err != nil {
		log.Fatal("Error update database with SQL Command -> " + dbSQL)
	}

	OneUser.username = username
	OneUser.fullname = fullname
	OneUser.telephone = telephone
	OneUser.address = address

	return ""
}

func updatePassword(username string, password string, w http.ResponseWriter, r *http.Request) string {
	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	password = hashPassword(password)
	dbSQL := "update users set password = '" + password + "' where username = '" + username + "'"
	results, err := dbCN.Exec(dbSQL)
	_ = results
	if err != nil {
		log.Fatal("Error update database with SQL Command -> " + dbSQL)
	}

	return ""
}

func prepareResetLink(username string, w http.ResponseWriter, r *http.Request) string {
	dbCN, err := sql.Open("mysql", ConnectionString)
	if err != nil {
		log.Fatal("Cannot connect to database! Please check connection string -> " + ConnectionString)
	}

	linkValue := hashLink(username)
	dbSQL := "update users set hashlink = '" + linkValue + "', expires =  DATE_ADD(NOW(), INTERVAL 1 HOUR) where username = '" + username + "'"
	results, err := dbCN.Exec(dbSQL)
	_ = results
	if err != nil {
		log.Fatal("Error update database with SQL Command -> " + dbSQL)
	}

	return linkValue
}
