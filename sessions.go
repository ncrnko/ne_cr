package main

import (
	"context"
	"log"
	"net/http"

	session "github.com/go-session/session/v3"
)

func sessionSave(key string, value string, w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(context.Background(), w, r)
	if err != nil {
		log.Println("sessionSaveError1 -> " + err.Error())
		return
	}
	store.Set(key, value)
	err = store.Save()
	if err != nil {
		return
	}
}

func sessionRead(key string, w http.ResponseWriter, r *http.Request) string {
	store, err := session.Start(context.Background(), w, r)
	if err != nil {
		log.Println("SessionReadError1" + err.Error())
		return ""
	}
	value, ok := store.Get(key)
	if ok {
		return value.(string)
	} else {
		return ""
	}
}

func sessionDestroy(w http.ResponseWriter, r *http.Request) {
	store, err := session.Start(context.Background(), w, r)
	if err != nil {
		log.Println("SessionDestroyError1" + err.Error())
		return
	}
	store.Delete("username")
}
